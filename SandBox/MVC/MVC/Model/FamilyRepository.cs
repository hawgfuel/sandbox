﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;

namespace MVC.Model
{
    /// <summary>
    /// Stores the data in a json file so that no database is required for this
    /// sample application
    /// </summary>
    public class FamilyRepository
    {
        /// <summary>
        /// Creates a new family with default values
        /// </summary>
        /// <returns></returns>
        internal Family Create()
        {
            Family family = new Family
            {
                //ReleaseDate = DateTime.Now
            };
            return family;
        }

        /// <summary>
        /// Retrieves the list of familys.
        /// </summary>
        /// <returns></returns>
        internal List<Family> Retrieve()
        {
            var filePath = HostingEnvironment.MapPath(@"~/App_Data/family.json");

            var json = System.IO.File.ReadAllText(filePath);

            var familys = JsonConvert.DeserializeObject<List<Family>>(json);

            return familys;
        }

        /// <summary>
        /// Saves a new family.
        /// </summary>
        /// <param name="family"></param>
        /// <returns></returns>
        internal Family Save(Family family)
        {
            // Read in the existing familys
            var familys = this.Retrieve();

            // Assign a new Id
            //var maxId = familys.Max(p => p.FamilyId);
            //family.FamilyId = maxId + 1;
            familys.Add(family);

            WriteData(familys);
            return family;
        }

        /// <summary>
        /// Updates an existing family
        /// </summary>
        /// <param name="id"></param>
        /// <param name="family"></param>
        /// <returns></returns>
        internal Family Save(int id, Family family)
        {
            // Read in the existing familys
            var familys = this.Retrieve();

            // Locate and replace the item
            //var itemIndex = familys.FindIndex(p => p.FamilyId == family.FamilyId);
            //if (itemIndex > 0)
            //{
            //    familys[itemIndex] = family;
            //}
            //else
            //{
            //    return null;
            //}

            WriteData(familys);
            return family;
        }

        private bool WriteData(List<Family> familys)
        {
            // Write out the Json
            var filePath = HostingEnvironment.MapPath(@"~/App_Data/family.json");

            var json = JsonConvert.SerializeObject(familys, Formatting.Indented);
            System.IO.File.WriteAllText(filePath, json);

            return true;
        }

    }
}
