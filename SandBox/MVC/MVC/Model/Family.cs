﻿
using System;

namespace MVC.Model
{
    public class Family
    {
        public string Parents { get; set; }
        public string DescendantNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public string MarriedTo { get; set; }
        public string MarriedDate { get; set; }
        public string History { get; set; }
    }
}