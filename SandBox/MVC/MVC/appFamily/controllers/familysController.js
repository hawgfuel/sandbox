﻿(function (app) {
   "use strict";
    var familysController = function ($scope, familyService) {

        var onError = function (q) {console.log("error= " + q);}
        var onFamily = function (response) {
            $scope.familys = response.data;

            // filter number of familys displayed
            var familyLength = $scope.familys.length;
            $scope.limitVal = "10";
            $scope.limitRange = [];
            for (var i = (0 - familyLength);
                i <= familyLength; i++) {
                if (i > 0) { $scope.limitRange.push(i.toString()); }
            }// end number of familys filter

            // isMarried Filter
            //$scope.family.isMarried = "married";
            //$scope.marriedRange = [];
            //for (var i = (0 - familyLength) ;
            //    i <= familyLength; i++) {
            //    if (i > 0 && family.marriedDate) { $scope.family.isMarried = true; }
            //}
            // End isMarried Filter
        }

        var init = function () {
            $scope.familys = familyService.getFamily().then(onFamily, onError);
            $scope.description = "Werstler Family tree";
        };
       
        // modal is not active
        $scope.isActive = false;
       // toggle modal visibility
        $scope.showModal = function (e) {
            $scope.isActive = !$scope.isActive;
            // change click arrow
            switch ($scope.isActive) {

                case true:
                    // save clickedElement
                    var clickedElement = e.target;
                    // save clicked descendant 
                    var showInModal = $(clickedElement).parent('.descendant');
                    // clone descendant clicked to display modal dialogue
                    showInModal = angular.copy(showInModal);
                    $('#modalBG').empty();
                    // insert copied descendant into the modalBG
                    $('#modalBG').append(showInModal).show();
                    $(showInModal).addClass("openDialogue");
                    break;

                case false:
                    $('#modalBG').empty();
                    $(showInModal).removeClass("openDialogue");
                    break;
            }
        }; 
        init();
    };
   
    // getting existing module
    app.controller("familysController", ["$scope", "familyService", familysController]);

})(angular.module("appFamily"));