﻿(function (app) {
    "use strict";
    app.directive("descendant", function(){
        return{
            restrict: "E",
            replace: true,
            templateUrl:"/appFamily/Templates/descendantTemplate.html"
        }
    });
})(angular.module("appFamily"));