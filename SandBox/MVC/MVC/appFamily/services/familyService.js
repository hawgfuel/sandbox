﻿(function (app) {
    "use strict";
    var baseUrl = "/api/familys";
    var familyService = function ($http) {
        var familyFactory = {};

        familyFactory.getFamily = function () {
            return $http.get(baseUrl);
        };

        return familyFactory;
    };
    app.factory("familyService", ["$http", familyService]);
}(angular.module("appFamily")));