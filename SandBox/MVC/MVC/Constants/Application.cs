﻿namespace MVC.Constants
{
    public class Application
    {
        public const string Name = "MVC 5/Angular";
        public const string ShortName = "Sandbox";
    }
}