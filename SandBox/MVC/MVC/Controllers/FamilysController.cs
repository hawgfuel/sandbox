﻿
using System.Collections.Generic;
using System.Web.Http;
using MVC.Model;

namespace MVC.Controllers
{
    public class FamilysController : ApiController
    {
        public IHttpActionResult GetFamilys() {
            var Familys = new List<Family>
            {
                new Family() { Parents="001", DescendantNumber="100", FirstName="Johannes", MiddleName="", LastName="Worschler", BirthDate="1711", MarriedTo="Maria Barbara Deischer", MarriedDate="Oct 28, 1738", History=" They emigrated to the America arriving in Philadelphia on board the Marlboro on Sep 23, 1741.Johannes settled in Colebrookdale Township which had just been established that year as a part of Philadelphia county.In 1747 Richard and Thomas Penn, sons of William Penn, granted a patent (deed) to Johannes for 65 acres of land.The site of this property was along County Line Road between Swamp Creek Road and present day Rt 100(as of 2013.)Over time additional land was added to the property.This farm remained in the Werstler family for more than 150 years.It was sold by the executors of the Jacob Werstler estate to Horace Deysher in 1901.Among their children Maria and Johannes had 5 sons.Christian inherited the homestead and paid his siblings for their shares. Johannes died in 1790."},
                new Family() { Parents="001", DescendantNumber="201", FirstName="Moritz", MiddleName="", LastName="Worschler", BirthDate="1752", MarriedTo="", MarriedDate="", History="Leo thought Moritz was a school teacher and had moved to Baltimore."},
                new Family() { Parents="001", DescendantNumber="202", FirstName="Johann", MiddleName="Wilhelm", LastName="Worschler", BirthDate="", MarriedTo="", MarriedDate="", History=""},
                new Family() { Parents="001", DescendantNumber="203", FirstName="Maria", MiddleName="", LastName="Worschler", BirthDate="", MarriedTo="", MarriedDate="", History=""},
                new Family() { Parents="001", DescendantNumber="204", FirstName="Johann", MiddleName="George", LastName="Worschler", BirthDate="June 28, 1722", MarriedTo="", MarriedDate="", History="baptized June 29"},
                new Family() { Parents="001", DescendantNumber="205", FirstName="Johann", MiddleName="Heinrich", LastName="Worschler", BirthDate="Mar 1, 1729", MarriedTo="Catharina Scheaffer", MarriedDate="June 21, 1750", History="In 1752 they emigrated to America.  Verbal  history tells us he lived in the area of Lancaster for a time. Eventually Johann settled in Stark County, Ohio.  The Stark County historical society lists him as one of the pioneers of the county. There is a replica of his frontier cabin in the Stark County museum.  He provided and helped to build a church which still bears his name.  People I have talked to from this part of Ohio tell me the Worschler name is prevalent in this area.  Although it is not spelled the same either."},
                new Family() { Parents="001", DescendantNumber="206", FirstName="Christian", MiddleName="", LastName="Worschler", BirthDate="", MarriedTo="Catherine", MarriedDate="", History="baptized Dec 26, 1770"},
                new Family() { Parents="001", DescendantNumber="206", FirstName="John", MiddleName="", LastName="Worschler", BirthDate="1743", MarriedTo="Margaret", MarriedDate="", History="John settled in Chester County, Pa. He died in 1778."},
                new Family() { Parents="001", DescendantNumber="207", FirstName="Henry", MiddleName="", LastName="Worschler", BirthDate="1743", MarriedTo="Margaret", MarriedDate="", History="Henry  and died at the age of 22."},
                new Family() { Parents="001", DescendantNumber="208", FirstName="George", MiddleName="", LastName="Worschler", BirthDate="1752", MarriedTo="Anna Maria Gulden", MarriedDate="1780", History="He settled in Chester County, Pa.  In 1794 he raised a company of militia during the Whiskey Rebellion.  He wore the rank of captain at Shippensburg, Pa.  He died May 14, 1832."},
                new Family() { Parents="205", DescendantNumber="300", FirstName="Johann", MiddleName="Heinrich", LastName="Worschler", BirthDate="Mar 1, 1757", MarriedTo="Catharina Scheaffer", MarriedDate="June 21, 1750", History="confirmed Easter 1777"},
                new Family() { Parents="205", DescendantNumber="301", FirstName="Margaretha", MiddleName="", LastName="Worschler", BirthDate="Feb 11, 1760", MarriedTo="", MarriedDate="", History="baptized May 25, 1760, confirmed 1777"},
                new Family() { Parents="205", DescendantNumber="302", FirstName="Catharina", MiddleName="", LastName="Worschler", BirthDate="Sep 13, 1762", MarriedTo="", MarriedDate="", History="baptized Oct 17, 1762, confirmed 1777"},
                new Family() { Parents="205", DescendantNumber="303", FirstName="Johann", MiddleName="George", LastName="Worschler", BirthDate="Feb 1, 1765", MarriedTo="", MarriedDate="", History="baptized April 28, 1768, confirmed 1777"},
                new Family() { Parents="205", DescendantNumber="304", FirstName="Elizabeth", MiddleName="", LastName="Worschler", BirthDate="July 1, 1767", MarriedTo="", MarriedDate="", History="baptized Dec 6, 1767"},
                new Family() { Parents="205", DescendantNumber="305", FirstName="Christian", MiddleName="", LastName="Worschler", BirthDate="Oct 2, 1770", MarriedTo="", MarriedDate="", History="baptized Dec 26, 1770"}
            };
            return Ok(Familys);
        }
    }
}
