﻿(function () {
    // Click events //
    // Click displays larger Descendant Modal
    $('div.descendant.small').on("click", function () { showDescendantModal(this); });

    // Click Modal should not do anything 
    $('div.descendant.openDialogue').on("click", function () { e.stopPropagation();});

    // Click dipslays larger Parent Modal
    // TODO: Create generic modal function
    $('a.seeParents').on("click", function () {
        var parents = getParents(this);
        focusParent(parents);
    });

    // Close/Hide modal transparent background
    $('#modalBG').on("click", function () {
        hideDescendantModal($data);
    });
     
    // Descendant Modal
    function showDescendantModal($data) {
        console.log('click');
        $('.descendant').removeClass('openDialogue');
        $('#modalBG').fadeIn();
        $($data).removeClass('small');
        $($data).addClass("openDialogue").fadeIn();
    }

    function hideDescendantModal(d) {
        $('.descendant').removeClass('openDialogue');
        $('#modalBG').fadeOut();
        $(descendantModal).addClass('small');
        $(descendantModal).removeClass("openDialogue").fadeOut();
    }

    // focal visual treatment of parent panel
    // TODO: need different visual treatment to show family parent from basic descendant
    function focusParent(d) { $(d).toggleClass("openDialogue");}

    // Get parent descendant number
    // this isn't working as expected
    // we want just the one parent with the descendant number
    function getParents(a) {
        var parents = $(a).parent().attr('data-parents');
        parents = $('#familyTree').find("div.descendant").css("data-descendantnumber", parents);
        console.log(parents.length);
        return parents;
    }

    // helper function
    function notUndefined(value) {
        if ((typeof value !== 'undefined') && (value !== '')) {
            return true;
        }
        else { return false; }
    }

    // random background color for descendant container
    function randomBackground() {
        var colorNumber = 1 + Math.floor(Math.random() * (4 - 1 + 1));
        var descBackground;

        switch (colorNumber) {
            case 1:
                descBackground = 'pale-green';
                break;
            case 2:
                descBackground = 'pale-green';
                break;
            case 3:
                descBackground = 'color3';
                break;
            default:
                descBackground = 'color4'
        }
        return descBackground;
    }
}());