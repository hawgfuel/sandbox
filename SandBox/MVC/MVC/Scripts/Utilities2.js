﻿$(document).ready(function () {

    var Todo = Backbone.Model.extend({});

    // We can then create our own concrete instance of a (Todo) model
    // with no values at all:
    var todo1 = new Todo();
    // Following logs: {}
    console.log(JSON.stringify(todo1));

    // or with some arbitrary data:
    var todo2 = new Todo({
        title: 'Check the attributes of both model instances in the console.',
        completed: true
    });

    console.log(JSON.stringify(todo2));


    var familyXML = new Array('j1.xml');
    var xmlLength = familyXML.length;
    console.log(xmlLength);

    for (var a = 0; a < xmlLength; a++) {
       // console.log('inside first for loop');
        // family tree ajax stuff
        var localServer = ""; // "http://localhost:51481/Z-PortfolioSite";
        var familySource = '/FamilyTree/xml/';
        var dataPath = localServer + familySource + familyXML[a];

        $.get(dataPath, function (d) {
            console.log('inside first get');
            var xmlDoc = $.parseXML(d);
            var familys = $(d).find('familyTree');

            $(familys).find('descendant').each(function () {
                  
                // Get XML values 
                var descendantNumberValue = $(this).find('descendantNumber').text();
                var parentValue = $(this).find('parents').text();
                var fistnameValue = $(this).find('firstname').text();
                var marriedValue = $(this).find('married').text();
                var lastNameValue = $(this).find('lastname').text();
                var historyValue = $(this).find('history').text();
                var birthdateValue = $(this).find('birthdate').text();

                // Create a descendant parent container
                var aDescendant = document.createElement('div');
                aDescendant.className = 'descendant';

                aDescendant.setAttribute("data-descendantNumber", descendantNumberValue);
                aDescendant.setAttribute("data-parents", parentValue);

                // Descendant firstname
                if (notUndefined(fistnameValue)) {
                    var firstname = document.createElement('span');
                        firstname.className = 'firstname';
                        firstname.appendChild(document.createTextNode(fistnameValue));
                        aDescendant.appendChild(firstname);
                    }

                // Descendant lastName
                if (notUndefined(lastNameValue)) {
                    var lastname = document.createElement('span');
                        lastname.className = 'lastName';
                        lastname.appendChild(document.createTextNode(lastNameValue));
                        aDescendant.appendChild(lastname);
                }

                // Descendant birthdate
                if (notUndefined(birthdateValue)) {
                    var birthdate = document.createElement('span');
                    birthdate.className = 'birthdate';
                    birthdate.appendChild(document.createTextNode('birthdate: ' + birthdateValue));
                    aDescendant.appendChild(birthdate);
                }

                // Descendant married
                if (notUndefined(marriedValue)) {
                    var married = document.createElement('span');
                        married.className = 'married';
                        married.appendChild(document.createTextNode('married: ' + marriedValue));
                        aDescendant.appendChild(married);
                }

                // Descendant history
                if (notUndefined(historyValue)) {
                    var history = document.createElement('span');
                        history.className = 'history';
                        history.appendChild(document.createTextNode(historyValue));
                        aDescendant.appendChild(history);
                }


                document.getElementById('familys').appendChild(aDescendant);

            }); // end each descendant

            $('div.family').on("click", function () {
                console.log('family click');
                $(this).addClass("openDialogue");
            });

        });

    }

    // basic helper function
    function notUndefined(value) {
        if ((typeof value !== 'undefined')&&(value!=='')) {
            return true;
        }
        else { return false; }
    }

});