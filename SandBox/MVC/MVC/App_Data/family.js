{
    "familyTree"; {
        "descendant"; [
          {
              "parents": "J0",
              "descendantNumber": "001",
              "firstname": "Johannes",
              "lastname": "Worschler",
              "birthdate": "1711",
              "married": "Maria Barbara Deischer Oct 28, 1738",
              "history": "They emigrated to the America arriving in Philadelphia on board the Marlboro on Sep 23, 1741. Johannes settled in Colebrookdale Township which had just been established that year as a part of Philadelphia county. In 1747 Richard and Thomas Penn, sons of William Penn, granted a patent (deed) to Johannes for 65 acres of land.
                        The site of this property was along County Line Road between Swamp Creek Road and present day Rt 100 (as of 2013.) Over time additional land was added to the property.  This farm remained in the Werstler family for more than 150 years.   It was sold by the executors of the Jacob Werstler estate to Horace Deysher in 1901.  Among their children Maria and Johannes had 5 sons. Christian inherited the homestead and paid his siblings for their shares. Johannes died in 1790."
    },
{
        "parents": "001",
        "descendantNumber": "J2",
        "firstname": "Moritz",
        "lastname": "Worschler",
        "birthdate": "1752",
        "history": "Leo thought Moritz was a school teacher and had moved to Baltimore.  This was never authenticated as far as I know."
},
{
        "parents": "001",
        "descendantNumber": "J4",
        "firstname": "Johann Wilhelm",
        "lastname": "Worschler"
},
{
    "parents": "001",
    "descendantNumber": "J6",
    "firstname": "Maria",
    "lastname": "Worschler"
},
{
        "parents": "001",
        "descendantNumber": "J7",
        "firstname": "Johann George",
        "lastname": "Worschler",
        "birthdate": "June 28, 1722",
        "history": "baptized June 29"
},
{
    "parents": "001",
    "descendantNumber": "205",
    "firstname": "Johann Heinrich",
    "lastname": "Worschler",
    "birthdate": "Mar 1, 1729",
    "married": "Catharina Scheaffer June 21, 1750",
    "history": "In 1752 they emigrated to America.  Verbal  history tells us he lived in the area of Lancaster for a time. Eventually Johann settled in Stark County, Ohio.  The Stark County historical society lists him as one of the pioneers of the county. There is a replica of his frontier cabin in the Stark County museum.  He provided and helped to build a church which still bears his name.  People I have talked to from this part of Ohio tell me the Worschler name is prevalent in this area.  Although it is not spelled the same either."
},
{
        "parents": "205",
        "descendantNumber": "J9",
        "firstname": "Johann Heinrich",
        "lastname": "Worschler",
        "birthdate": "Aug 28, 1757",
        "history": "confirmed Easter 1777"
},
{
    "parents": "205",
    "descendantNumber": "0010",
    "firstname": "Margaretha",
    "lastname": "Worschler",
    "birthdate": "Feb11, 1760",
    "history": "baptized May 25, 1760, confirmed 1777"
},
{
        "parents": "205",
        "descendantNumber": "0011",
        "firstname": "Catharina",
        "lastname": "Worschler",
        "birthdate": "Sep 13, 1762",
        "history": "baptized Oct 17, 1762, confirmed 1777"
},
{
    "parents": "205",
    "descendantNumber": "0012",
    "firstname": "Johann George",
    "lastname": "Worschler",
    "birthdate": "Feb 1, 1765",
    "history": "baptized April 28, 1768, confirmed 1777"
},
{
        "parents": "205",
        "descendantNumber": "0013",
        "firstname": "Elizabeth",
        "lastname": "Worschler",
        "birthdate": "July 1, 1767",
        "history": "baptized Dec 6, 1767"
},
{
    "parents": "205",
    "descendantNumber": "0014",
    "firstname": "Christian",
    "lastname": "Worschler",
    "birthdate": "Oct 2, 1770",
    "history": "baptized Dec 26, 1770"
},
{
        "parents": "001",
        "descendantNumber": "0015",
        "firstname": "Christian",
        "lastname": "Worschler",
        "married": "Catherine"
},
{
    "parents": "001",
    "descendantNumber": "0016",
    "firstname": "John",
    "lastname": "Worschler",
    "married": "Margaret",
    "birthdate": "1743",
    "history": "John settled in Chester County, Pa. He died in 1778."
},
{
        "parents": "001",
        "descendantNumber": "0017",
        "firstname": "Henry",
        "lastname": "Worschler",
        "birthdate": "1743",
        "history": "Henry never married and died at the age of 22."
},
{
    "parents": "001",
    "descendantNumber": "0018",
    "firstname": "George",
    "lastname": "Worschler",
    "married": "Anna Maria Gulden in 1780.",
    "birthdate": "1752",
    "history": "He settled in Chester County, Pa.  In 1794 he raised a company of militia during the Whiskey Rebellion.  He wore the rank of captain at Shippensburg, Pa.  He died May 14, 1832."
},
{
        "parents": "001",
        "descendantNumber": "0019",
        "firstname": "Jacob",
        "lastname": "Worschler",
        "birthdate": "Nov 30, 1746",
        "married": "Catherine Shoen in 1772",
        "history": "He lived in the Boyertown area and died  in 1825."
},
{
    "parents": "001",
    "descendantNumber": "J3",
    "firstname": "Johann Christian",
    "lastname": "Worschler",
    "birthdate": "Jan 21, 1717",
    "history": "baptized Jan 24, 1717.  He died in Germany."
},
{
        "parents": "001",
        "descendantNumber": "J5",
        "firstname": "Anna Magdelana",
        "married": {
            "-parents": "Johannes and Anna Elizabeth Deischer",
            "#text": "Nicol Deischer"
        }
},
{
    "parents": "0015",
    "descendantNumber": "J20",
    "firstname": "Jacob",
    "lastname": "Werstler",
    "married": "Maria Harner July 24, 1814.",
    "birthdate": "July 21, 1785",
    "history": "A member of the Mennonite church in Boyertown, he died Dec 22, 1866.  He is buried in the Mennonite cemetery behind the National Penn bank  on North Reading Ave. in Boyertown. His grave is close to the wall which borders Good Shepherd U.C.C. church."
},
{
        "parents": "0015",
        "descendantNumber": "J21",
        "firstname": "Catherine",
        "lastname": "Werstler",
        "married": "Mathias Gilbert Dec 15, 1816."
},
{
    "descendantNumber": "J0",
    "firstname": "Johannes",
    "birthdate": "Nov 9, 1817",
    "married": "Maria Catherine Lutzen",
    "lastname": "Worschler"
},
{
        "parents": "0016",
        "descendantNumber": "J22",
        "firstname": "John",
        "lastname": "Wersler",
        "married": "Mary Werkizer",
        "history": "He died in 1822."
},
{
    "parents": "0015",
    "descendantNumber": "J23",
    "firstname": "Jacob",
    "lastname": "Wersler  Sr.",
    "married": "Barbara",
    "history": "He died in 1836."
},
{
        "parents": "0015",
        "descendantNumber": "J24",
        "firstname": "Henry",
        "lastname": "Wersler",
        "history": "Died in 1844."
},
{
    "parents": "J20",
    "descendantNumber": "J31",
    "firstname": "Jacob",
    "lastname": "Werstler",
    "birthdate": "Dec 6, 1819.  ",
    "history": "Never married. Died Dec 29, 1898."
},
{
        "parents": "J20",
        "descendantNumber": "J32",
        "firstname": "David",
        "lastname": "Werstler",
        "birthdate": "June 9, 1824",
        "history": "Never married. Died Dec 29, 1898."
},
{
    "parents": "J20",
    "descendantNumber": "J33",
    "firstname": "Jonas",
    "lastname": "Werstler",
    "married": "Rebecca Gilbert"
},
{
        "parents": "J20",
        "descendantNumber": "J34",
        "firstname": "William Caster",
        "lastname": "Werstler",
        "married": {
            "-birthdate": "Jan 11, 1822",
            "-died": "April 29, 1875",
            "#text": "Elizabeth"
        },
        "history": "Died Feb 10, 1908"
},
{
    "parents": "J20",
    "descendantNumber": "J36",
    "firstname": "Maria",
    "lastname": "Werstler",
    "married": "Joseph Butterwick"
},
{
        "parents": "J22",
        "descendantNumber": "J38",
        "firstname": "Jacob",
        "lastname": "Wersler"
},
{
    "parents": "J22",
    "descendantNumber": "J39",
    "firstname": "George",
    "lastname": "Wersler"
},
{
        "parents": "J22",
        "descendantNumber": "J41",
        "firstname": "Thomas",
        "lastname": "Wersler"
},
{
    "parents": "J23",
    "descendantNumber": "J42",
    "firstname": "John",
    "lastname": "Wersler",
    "married": "Rebecca"
},
{
        "parents": "0018",
        "descendantNumber": "J25",
        "firstname": "John G.",
        "lastname": "Wersler",
        "married": "Maria Davis",
        "birthdate": "Jan 30, 1781",
        "history": "Baptized Dec 20, 1775.  He married Maria Davis in 1810.  During the was of 1812 he recruited a company of volunteers which joined the 2nd regiment of Pennsylvania volunteers."
},
{
    "parents": "0018",
    "descendantNumber": "J26",
    "firstname": "Rebecca",
    "lastname": "Wersler",
    "birthdate": "Dec 20, 1785",
    "history": "Baptized Dec 20, 1785"
},
{
        "parents": "0018",
        "descendantNumber": "J27",
        "firstname": "Elisabeth",
        "lastname": "Wersler",
        "birthdate": "Dec 18, 1770",
        "history": "Baptized April 18, 1771."
},
{
    "parents": "0018",
    "descendantNumber": "J28",
    "firstname": "Maria",
    "lastname": "Wersler",
    "birthdate": "Feb 1778",
    "history": "Baptized Oct 29, 1782."
},
{
        "parents": "0019",
        "descendantNumber": "J29",
        "firstname": "Catharine",
        "lastname": "Wersler",
        "married": "Philip Rapp Nov 30, 1794."
},
{
    "parents": "0019",
    "descendantNumber": "J30",
    "firstname": "Jacob",
    "lastname": "Wersler",
    "married": "Catharine Harner",
    "birthdate": "Oct 22, 1785",
    "history": "Married Catharine Harner July 25, 1814.  Jacob operated a hotel at the corner of Swamp Pike and Gilbertsville Road. He died July 25, 1863.  He is buried in the Mennonite cemetery behind the National Penn bank . "
},
{
        "parents": "0018",
        "descendantNumber": "J47",
        "firstname": "Milton",
        "lastname": "Wersler",
        "birthdate": "Dec 14, 1811"
},
{
    "parents": "0018",
    "descendantNumber": "J48",
    "firstname": "Anna Maria",
    "lastname": "Wersler"
},
{
        "parents": "0018",
        "descendantNumber": "J49",
        "firstname": "George",
        "lastname": "Wersler"
},
{
    "parents": "0018",
    "descendantNumber": "J50",
    "firstname": "Hesekiah Davis",
    "lastname": "Wersler",
    "birthdate": "Feb 18, 1819"
},
{
        "parents": "0018",
        "descendantNumber": "J51",
        "firstname": "Harriet M.",
        "lastname": "Wersler",
        "birthdate": "Mar 12, 1820"
},
{
    "parents": "0018",
    "descendantNumber": "J52",
    "firstname": "Annie D.",
    "lastname": "Wersler"
},
{
        "parents": "0018",
        "descendantNumber": "J53",
        "firstname": "Albert Horatio",
        "lastname": "Wersler",
        "birthdate": "Jan 12, 1824"
},
{
    "parents": "0018",
    "descendantNumber": "J54",
    "firstname": "William",
    "lastname": "Wersler",
    "birthdate": "Dec 31, 1827"
}
]
}
}