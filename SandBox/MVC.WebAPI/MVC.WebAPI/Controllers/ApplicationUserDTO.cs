﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace MVC.WebAPI.Models
{
	public class ApplicationUserDTO
    {
		public int Claims_Count { get; set; }
		public int Logins_Count { get; set; }
		public int Roles_Count { get; set; }
		public System.String Id { get; set; }
		public System.String Email { get; set; }
		public System.Boolean EmailConfirmed { get; set; }
		public System.String PasswordHash { get; set; }
		public System.String SecurityStamp { get; set; }
		public System.String PhoneNumber { get; set; }
		public System.Boolean PhoneNumberConfirmed { get; set; }
		public System.Boolean TwoFactorEnabled { get; set; }
		public System.DateTime? LockoutEndDateUtc { get; set; }
		public System.Boolean LockoutEnabled { get; set; }
		public System.Int32 AccessFailedCount { get; set; }
		public System.String UserName { get; set; }

        public static System.Linq.Expressions.Expression<Func< ApplicationUser,  ApplicationUserDTO>> SELECT =
            x => new  ApplicationUserDTO
            {
                Claims_Count = x.Claims.Count(),
                Logins_Count = x.Logins.Count(),
                Roles_Count = x.Roles.Count(),
                Id = x.Id,
                Email = x.Email,
                EmailConfirmed = x.EmailConfirmed,
                PasswordHash = x.PasswordHash,
                SecurityStamp = x.SecurityStamp,
                PhoneNumber = x.PhoneNumber,
                PhoneNumberConfirmed = x.PhoneNumberConfirmed,
                TwoFactorEnabled = x.TwoFactorEnabled,
                LockoutEndDateUtc = x.LockoutEndDateUtc,
                LockoutEnabled = x.LockoutEnabled,
                AccessFailedCount = x.AccessFailedCount,
                UserName = x.UserName,
            };

	}
}