﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace MVC.WebAPI.Models
{
    public class ApplicationUserController : ApiController
    {
        private MVC.WebAPI.Models.ApplicationDbContext db = new MVC.WebAPI.Models.ApplicationDbContext();

        public IQueryable<ApplicationUserDTO> GetApplicationUsers(int pageSize = 10
                )
        {
            var model = db.ApplicationUsers.AsQueryable();
                        
            return model.Select(ApplicationUserDTO.SELECT).Take(pageSize);
        }

        [ResponseType(typeof(ApplicationUserDTO))]
        public async Task<IHttpActionResult> GetApplicationUser(string id)
        {
            var model = await db.ApplicationUsers.Select(ApplicationUserDTO.SELECT).FirstOrDefaultAsync(x => x.Id == id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        public async Task<IHttpActionResult> PutApplicationUser(string id, ApplicationUser model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(ApplicationUserDTO))]
        public async Task<IHttpActionResult> PostApplicationUser(ApplicationUser model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ApplicationUsers.Add(model);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ApplicationUserExists(model.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            var ret = await db.ApplicationUsers.Select(ApplicationUserDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return CreatedAtRoute("DefaultApi", new { id = model.Id }, model);
        }

        [ResponseType(typeof(ApplicationUserDTO))]
        public async Task<IHttpActionResult> DeleteApplicationUser(string id)
        {
            ApplicationUser model = await db.ApplicationUsers.FindAsync(id);
            if (model == null)
            {
                return NotFound();
            }

            db.ApplicationUsers.Remove(model);
            await db.SaveChangesAsync();
            var ret = await db.ApplicationUsers.Select(ApplicationUserDTO.SELECT).FirstOrDefaultAsync(x => x.Id == model.Id);
            return Ok(ret);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationUserExists(string id)
        {
            return db.ApplicationUsers.Count(e => e.Id == id) > 0;
        }
    }
}