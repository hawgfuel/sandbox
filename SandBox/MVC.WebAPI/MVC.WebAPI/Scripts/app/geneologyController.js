﻿// geneologyController.js
(function (app) {
    // "use strict";

    var geneologyController = function ($scope) {
        $scope.description = "Werstler Family tree";
    };

    // getting existing module
    app.controller("geneologyController", ["$scope", geneologyController]);

})(angular.module("appGeneology"));