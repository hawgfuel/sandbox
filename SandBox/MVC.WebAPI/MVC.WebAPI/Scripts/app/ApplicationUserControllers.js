﻿ var  modules = modules || [];
(function () {
    'use strict';
    modules.push('ApplicationUser');

    angular.module('ApplicationUser',['ngRoute'])
    .controller('ApplicationUser_list', ['$scope', '$http', function($scope, $http){

        $http.get('/Api/ApplicationUser/')
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('ApplicationUser_details', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

        $http.get('/Api/ApplicationUser/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

    }])
    .controller('ApplicationUser_create', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $scope.data = {};
        
        $scope.save = function(){
            $http.post('/Api/ApplicationUser/', $scope.data)
            .then(function(response){ $location.path("ApplicationUser"); });
        }

    }])
    .controller('ApplicationUser_edit', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/ApplicationUser/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});

        
        $scope.save = function(){
            $http.put('/Api/ApplicationUser/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("ApplicationUser"); });
        }

    }])
    .controller('ApplicationUser_delete', ['$scope', '$http', '$routeParams', '$location', function($scope, $http, $routeParams, $location){

        $http.get('/Api/ApplicationUser/' + $routeParams.id)
        .then(function(response){$scope.data = response.data;});
        $scope.save = function(){
            $http.delete('/Api/ApplicationUser/' + $routeParams.id, $scope.data)
            .then(function(response){ $location.path("ApplicationUser"); });
        }

    }])

    .config(['$routeProvider', function ($routeProvider) {
            $routeProvider
            .when('/ApplicationUser', {
                title: 'ApplicationUser - List',
                templateUrl: '/Static/ApplicationUser_List',
                controller: 'ApplicationUser_list'
            })
            .when('/ApplicationUser/Create', {
                title: 'ApplicationUser - Create',
                templateUrl: '/Static/ApplicationUser_Edit',
                controller: 'ApplicationUser_create'
            })
            .when('/ApplicationUser/Edit/:id', {
                title: 'ApplicationUser - Edit',
                templateUrl: '/Static/ApplicationUser_Edit',
                controller: 'ApplicationUser_edit'
            })
            .when('/ApplicationUser/Delete/:id', {
                title: 'ApplicationUser - Delete',
                templateUrl: '/Static/ApplicationUser_Delete',
                controller: 'ApplicationUser_delete'
            })
            .when('/ApplicationUser/:id', {
                title: 'ApplicationUser - Details',
                templateUrl: '/Static/ApplicationUser_Details',
                controller: 'ApplicationUser_details'
            })
    }])
;

})();
