﻿$(document).ready(function () {

    // family tree ajax stuff
    var localServer = "";
    var familySource = '/Xml/';
    var familyXML = new Array('gen1.xml');
    var dataPath = localServer + familySource + familyXML;
    getData();

    function getData() {
        $.ajax({
            url: dataPath,
            type: 'GET',
            success: buildTree
        });
    }

    function buildTree(data) {
        console.log('inside buildTree');
        var $data = data;
        var xmlDoc = $.parseXML($data);
        var familys = $($data).find('familyTree');

        $(familys).find('descendant').each(function () {
            console.log('inside familys for each');
            // Get XML values 
            var descendantNumberValue = $(this).find('descendantNumber').text();
            var parentValue = $(this).find('parents').text();
            var fistnameValue = $(this).find('firstname').text();
            var marriedValue = $(this).find('married').text();
            var lastNameValue = $(this).find('lastname').text();
            var historyValue = $(this).find('history').text();
            var birthdateValue = $(this).find('birthdate').text();

            // Create a descendant parent container
            var aDescendant = document.createElement('div');
            aDescendant.className = 'descendant small';

            // $('.descendant:first-child').addClass('openDialogue color1');

            // Descendant firstname
            if (notUndefined(fistnameValue)) {
                var firstname = document.createElement('span');
                firstname.className = 'firstname';
                firstname.appendChild(document.createTextNode(fistnameValue));
                aDescendant.appendChild(firstname);
            }

            // Descendant lastName
            if (notUndefined(lastNameValue)) {
                var lastname = document.createElement('span');
                lastname.className = 'lastname';
                lastname.appendChild(document.createTextNode(lastNameValue));
                aDescendant.appendChild(lastname);
            }

            // Descendant birthdate
            if (notUndefined(birthdateValue)) {
                var birthdate = document.createElement('span');
                birthdate.className = 'birthdate';
                birthdate.appendChild(document.createTextNode('Birthdate: ' + birthdateValue));
                aDescendant.appendChild(birthdate);
            }

            // Descendant married
            if (notUndefined(marriedValue)) {
                var married = document.createElement('span');
                married.className = 'married';
                married.appendChild(document.createTextNode('Married: ' + marriedValue));
                aDescendant.appendChild(married);
            }

            // Descendant history
            if (notUndefined(historyValue)) {
                var history = document.createElement('p');
                history.className = 'history';
                history.appendChild(document.createTextNode(historyValue));
                aDescendant.appendChild(history);
            }

            if (notUndefined(descendantNumberValue)) {
                aDescendant.setAttribute("data-descendantNumber", descendantNumberValue);
                aDescendant.setAttribute("data-parents", parentValue);
                var seeParents = document.createElement('a');
                seeParents.className = 'seeParents';
                seeParents.appendChild(document.createTextNode('See Parents'));
                aDescendant.appendChild(seeParents);
            }
            document.getElementById('familys').appendChild(aDescendant);

        }); // end each descendant

        // Click events //
        // Click displays larger Descendant Modal
        $('div.descendant.small').on("click", function () { showDescendantModal(this); });

        // Click Modal should not do anything 
        $('div.descendant.openDialogue').on("click", function () { e.stopPropagation(); });

        // Click dipslays larger Parent Modal
        // TODO: Create generic modal function
        $('a.seeParents').on("click", function () {
            var parents = getParents(this);
            focusParent(parents);
        });

        // Close/Hide modal transparent background
        $('#modalBG').on("click", function () {
            hideDescendantModal($data);
        });

        // Descendant Modal
        function showDescendantModal($data) {
            $('.descendant').removeClass('openDialogue');
            $('#modalBG').fadeIn();
            $($data).removeClass('small');
            $($data).addClass("openDialogue").fadeIn();
        }

        function hideDescendantModal(d) {
            $('.descendant').removeClass('openDialogue');
            $('#modalBG').fadeOut();
            $(descendantModal).addClass('small');
            $(descendantModal).removeClass("openDialogue").fadeOut();
        }

        // focal visual treatment of parent panel
        // TODO: need different visual treatment to show family parent from basic descendant
        function focusParent(d) { $(d).toggleClass("openDialogue"); }

        // Get parent descendant number
        // this isn't working as expected
        // we want just the one parent with the descendant number
        function getParents(a) {
            var parents = $(a).parent().attr('data-parents');
            parents = $('#familys').find("div.descendant").css("data-descendantnumber", parents);
            console.log(parents.length);
            return parents;
        }

        // helper function
        function notUndefined(value) {
            if ((typeof value !== 'undefined') && (value !== '')) {
                return true;
            }
            else { return false; }
        }

        // random background color for descendant container
        function randomBackground() {
            var colorNumber = 1 + Math.floor(Math.random() * (4 - 1 + 1));
            var descBackground;

            switch (colorNumber) {
                case 1:
                    descBackground = 'pale-green';
                    break;
                case 2:
                    descBackground = 'pale-green';
                    break;
                case 3:
                    descBackground = 'color3';
                    break;
                default:
                    descBackground = 'color4'
            }
            return descBackground;
        }
    }   // end of buildTree
});